import os
import abc
import torch
import numpy as np

from typing import Optional


class AbstractModelInferencer(abc.ABC):
    def __init__(self, weights: str, device: Optional[str]):
        if not os.path.exists(weights):
            raise ValueError(f'No weights file found {weights}')
        elif os.path.splitext(weights)[-1] not in ['.pth', '.pt']:
            raise ValueError(f'Weights file {weights} not supported.\nPlease use .pth or .pt file')
        else:
            self.model_weights = weights
        if device:
            self.device = device
        else:
            self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

        self.alpha_mask: Optional[np.array] = None
        self.process_time: Optional[float] = None

    @abc.abstractmethod
    def _load_model(self) -> None:
        pass

    @abc.abstractmethod
    def single_inference(self, image: np.array, **kwargs) -> np.array:
        pass

    @abc.abstractmethod
    def get_alpha_mask(self) -> np.array:
        pass

    @abc.abstractmethod
    def get_process_time(self) -> float:
        pass
