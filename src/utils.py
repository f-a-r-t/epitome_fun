import cv2 


def generate_composite_img(img, alpha_channel):
    b_channel, g_channel, r_channel = cv2.split(img)
    b_channel = b_channel * alpha_channel
    g_channel = g_channel * alpha_channel
    r_channel = r_channel * alpha_channel
    img_BGR = cv2.merge((b_channel, g_channel, r_channel))
    return img_BGR
