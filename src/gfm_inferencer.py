import time
import torch
import warnings
import numpy as np

from typing import Optional, Tuple
from skimage.transform import resize

from .utils import generate_composite_img
from .abstract_model_inferencer import AbstractModelInferencer

from modules.GFM.core.gfm import GFM
from modules.GFM.core.util import get_masked_local_from_global_test, gen_trimap_from_segmap_e2e


class GFMInferencer(AbstractModelInferencer):
    """
    https://github.com/JizhiziLi/GFM
    """
    def __init__(self, weights: str, device: Optional[str] = None):
        super().__init__(weights, device)
        self._load_model()
    
    def _load_model(self):
        try:
            self.model = GFM('r34', 'TT').to(self.device)
            self.model.load_state_dict(torch.load(self.model_weights, map_location=self.device))
            self.model.eval()
        except Exception:
            raise ValueError(f'Could not load GFM model.\nPlease check if weights {self.model_weights} are set correctly')
    
    def get_alpha_mask(self) -> np.array:
        if self.alpha_mask is None:
            raise ValueError('Please run inference on image to retrieve the alpha mask')
        else:
            return (self.alpha_mask * 255).astype('uint8')

    def get_process_time(self) -> np.array:
        if self.process_time:
            return self.process_time
        else:
            raise ValueError('Please run inference on image to retrieve the mask')
    
    def single_inference(self,  image: np.array, **kwargs) -> np.array:
        threshold = kwargs.get('threshold', None)

        start_time = time.time()
        h, w, _ = image.shape
        new_h = h - (h % 32)
        new_w = w - (w % 32)
        global_ratio = 1/3
        local_ratio = 1/2
        resize_h = int(h*global_ratio)
        resize_w = int(w*global_ratio)
        new_h = resize_h - (resize_h % 32)
        new_w = resize_w - (resize_w % 32)
        scale_img = resize(image,(new_h, new_w))*255.0
        pred_glance_1, _, _ = self.inference_img_scale(scale_img)
        pred_glance_1 = resize(pred_glance_1,(h, w))*255.0
        resize_h = int(h*local_ratio)
        resize_w = int(w*local_ratio)
        new_h = resize_h - (resize_h % 32)
        new_w = resize_w - (resize_w % 32)
        scale_img = resize(image,(new_h, new_w))*255.0
        _, pred_focus_2, _ = self.inference_img_scale(scale_img)
        pred_focus_2 = resize(pred_focus_2,(h, w))
        self.alpha_mask = get_masked_local_from_global_test(pred_glance_1, pred_focus_2)
        
        if threshold:
            if threshold > 0.01 and threshold < 0.99:
                self.alpha_mask[self.alpha_mask <= threshold] = 0
            else:
                warnings.warn('Please use threshold in [0.01 - 0.99] range')
        
        out_img = generate_composite_img(image, self.alpha_mask)
        self.process_time = time.time() - start_time
        return out_img
        
    def inference_img_scale(self, scale_img: np.array) -> Tuple[np.array, np.array, np.array]:
        tensor_img = torch.from_numpy(scale_img.astype(np.float32)[np.newaxis, :, :, :]).permute(0, 3, 1, 2)
        tensor_img = tensor_img.to(self.device)
        pred_global, pred_local, pred_fusion = self.model(tensor_img)
        pred_global = pred_global.data.cpu().numpy()
        pred_global = gen_trimap_from_segmap_e2e(pred_global)
        pred_local = pred_local.data.cpu().numpy()[0,0,:,:]
        pred_fusion = pred_fusion.data.cpu().numpy()[0,0,:,:]
        return pred_global, pred_local, pred_fusion
