import time
import torch 
import warnings
import numpy as np

from typing import Optional
from torchvision.transforms import ToTensor

from .utils import generate_composite_img
from .abstract_model_inferencer import AbstractModelInferencer
from modules.RobustVideoMatting.model import MattingNetwork


class RobustMattingInferencer(AbstractModelInferencer):
    """
    https://github.com/PeterL1n/RobustVideoMatting
    """
    def __init__(self, weights: str, device: Optional[str] = None):
        super().__init__(weights, device)
        self._load_model()

    def _load_model(self) -> None:
        try:
            self.model = MattingNetwork('resnet50').to(self.device)
            self.model.load_state_dict(torch.load(self.model_weights, map_location=self.device))
            self.model.eval()
        except Exception:
            raise ValueError(f'Could not load RobustMatting model.\nPlease check if weights {self.model_weights} are set correctly')
        
    def get_alpha_mask(self) -> np.array:
        if self.alpha_mask is None:
            raise ValueError('Please run inference on image to retrieve the alpha mask')
        else:
            return (self.alpha_mask*255).astype('uint8')

    def get_process_time(self) -> np.array:
        if self.process_time:
            return self.process_time
        else:
            raise ValueError('Please run inference on image to retrieve the mask')

    def single_inference(self, image: np.array, **kwargs) -> np.array:
        threshold = kwargs.get('threshold', None)

        start_time = time.time()
        with torch.no_grad():
            transforms = ToTensor()
            img = transforms(image).to(self.device)
            img = torch.unsqueeze(img, 0)
            rec = [None] * 4                                      
            downsample_ratio = 0.25  
            _, pha, *rec = self.model(img, *rec, downsample_ratio)
            self.alpha_mask = torch.squeeze(torch.squeeze(pha.cpu().permute(0, 2, 3, 1), 0), -1).numpy()
            if threshold:
                if threshold > 0.01 and threshold < 0.99:
                    self.alpha_mask[self.alpha_mask <= threshold] = 0
                else:
                    warnings.warn('Please use threshold in [0.01 - 0.99] range')
                
            out_img = generate_composite_img(image, self.alpha_mask)
                    
        self.process_time = time.time() - start_time
        return out_img
