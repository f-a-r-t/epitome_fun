import cv2
import time
import torch
import warnings
import numpy as np

from typing import Optional
from torchvision.transforms import Normalize

from .utils import generate_composite_img
from .abstract_model_inferencer import AbstractModelInferencer

from modules.stylematte.models import StyleMatte


class StyleMatteInferencer(AbstractModelInferencer):
    """
    https://github.com/chroneus/stylematte
    """
    def __init__(self, weights: str, device: Optional[str] = None):
        super().__init__(weights, device)
        self._load_model()
    
    def _load_model(self):
        try:
            self.model = StyleMatte().to(self.device)
            self.model.load_state_dict(torch.load(self.model_weights, map_location=self.device))
            self.model.eval()
        except Exception:
            raise ValueError(f'Could not load StyleMate model.\nPlease check if weights {self.model_weights} are set correctly')
    
    def get_alpha_mask(self) -> np.array:
        if self.alpha_mask is None:
            raise ValueError('Please run inference on image to retrieve the alpha mask')
        else:
            return (self.alpha_mask * 255).astype('uint8')

    def get_process_time(self) -> np.array:
        if self.process_time:
            return self.process_time
        else:
            raise ValueError('Please run inference on image to retrieve the mask')
    
    def single_inference(self,  image: np.array,  **kwargs) -> np.array:
        threshold = kwargs.get('threshold', None)
        start_time = time.time()
        h, w, _ = image.shape
        if h % 8 != 0 or w % 8 != 0:
            img = cv2.copyMakeBorder(image, 8-h % 8, 0, 
                                       8-w % 8,0, cv2.BORDER_REFLECT)
        else:
            img = image
        tensor_img = torch.from_numpy(img).permute(2, 0, 1).to(self.device)
        input_t = tensor_img
        input_t = input_t/255.0
        transforms = Normalize(mean=[0.485, 0.456, 0.406],
                               std=[0.229, 0.224, 0.225])
        input_t = transforms(input_t)
        input_t = input_t.unsqueeze(0).float()
        with torch.no_grad():
            out = self.model(input_t)
        msk = torch.squeeze(torch.squeeze(out.cpu().permute(0, 2, 3, 1), 0), -1).numpy()
        self.alpha_mask = cv2.resize(msk, (w, h))
        
        if threshold:
            if threshold > 0.01 and threshold < 0.99:
                self.alpha_mask[self.alpha_mask <= threshold] = 0
            else:
                warnings.warn('Please use threshold in [0.01 - 0.99] range')
            
        out_img = generate_composite_img(image, self.alpha_mask)
        self.process_time = time.time() - start_time
        return out_img
