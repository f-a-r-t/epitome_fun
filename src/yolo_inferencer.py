import cv2
import time
import warnings
import numpy as np

from typing import Optional
from enum import IntEnum
from ultralytics import YOLO
from pymatting import estimate_alpha_cf

from .utils import generate_composite_img
from .abstract_model_inferencer import AbstractModelInferencer


class YoloMaskMergeTypes(IntEnum):
    MAX_SCORE = 0
    MERGE = 1


class YoloModelInferencer(AbstractModelInferencer):
    """
    https://github.com/ultralytics/ultralytics
    """
    def __init__(self, weights: str, device: Optional[str] = None):
        super().__init__(weights, device)
        self._load_model()

    def _load_model(self) -> None:
        try:
            self.model = YOLO(self.model_weights)
        except Exception:
            raise ValueError(f'Could not load Yolo model.\nPlease check if weights {self.model_weights} are set correctly')
    
    def get_alpha_mask(self) -> np.array:
        if self.alpha_mask is None:
            raise ValueError('Please run inference on image to retrieve the alpha mask')
        else:
            return (self.alpha_mask * 255).astype('uint8')

    def get_process_time(self) -> np.array:
        if self.process_time:
            return self.process_time
        else:
            raise ValueError('Please run inference on image to retrieve the mask')
    
    def single_inference(self, image: np.array, **kwargs) -> np.array:
        generate_alpha = kwargs.get('generate_alpha', False)
        threshold = kwargs.get('threshold', None)

        start_time = time.time()
        pred = self.model(image)
        h, w = pred[0].orig_shape
        seg_pred = pred[0].masks
        self.alpha_mask = np.zeros((h, w), dtype=np.uint8)
        
        if seg_pred:
            for i, sp in enumerate(seg_pred):
                cv2.fillPoly(self.alpha_mask, np.int32(sp.xy), (1))            
            if generate_alpha:
                msk = self.alpha_mask*255
                trimap = self._generate_trimap(msk)
                self.alpha_mask = self._generate_alpha(image, trimap)
                if threshold:
                    if threshold > 0.01 and threshold < 0.99:
                        self.alpha_mask[self.alpha_mask <= threshold] = 0
                    else:
                        warnings.warn('Please use threshold in [0.01 - 0.99] range')
            out_img = generate_composite_img(image, self.alpha_mask)
        else: 
            warnings.warn('No object was detected!')
            out_img = image
        self.process_time = time.time() - start_time
        
        return out_img
            
    def class_based_inference(self, image: np.array, **kwargs) -> np.array:
        threshold = kwargs.get('threshold', None)
        generate_alpha = kwargs.get('generate_alpha', False)
        merge_type = kwargs.get('merge_type', YoloMaskMergeTypes.MERGE)
        cls = kwargs.get('cls', 15)

        start_time = time.time()

        pred = self.model(image)
        h, w = pred[0].orig_shape
        det_pred = pred[0].boxes
        preds = det_pred.cls.numpy()
        self.alpha_mask = np.zeros((h, w), dtype=np.uint8)

        if merge_type == 0:
            try:
                idx = list(preds).index(cls)
                seg_pred = pred[0].masks[idx]
                cv2.fillPoly(self.alpha_mask, np.int32(seg_pred.xy), (1))
                if generate_alpha:
                    msk = self.alpha_mask*255
                    trimap = self._generate_trimap(msk)
                    self.alpha_mask = self._generate_alpha(image, trimap)
                    if threshold:
                        if threshold > 0.01 and threshold < 0.99:
                            self.alpha_mask[self.alpha_mask <= threshold] = 0
                        else:
                            warnings.warn('Please use threshold in [0.01 - 0.99] range')
                out_img = generate_composite_img(image, self.alpha_mask)
            except Exception:
                warnings.warn(f'No detection found for class {cls}')
                out_img = image
        elif merge_type == 1:
            try:
                idx = np.where(preds == cls)[0]
                for i in idx:
                    cv2.fillPoly(self.alpha_mask, np.int32( pred[0].masks[i].xy), (1))
                if generate_alpha:
                    msk = self.alpha_mask*255
                    trimap = self._generate_trimap(msk)
                    self.alpha_mask = self._generate_alpha(image, trimap)
                    if threshold:
                        if threshold > 0.01 and threshold < 0.99:
                            self.alpha_mask[self.alpha_mask <= threshold] = 0
                        else:
                            warnings.warn('Please use threshold in [0.01 - 0.99] range')
                out_img = generate_composite_img(image, self.alpha_mask)
            except Exception:
                warnings.warn(f'No detection found for class {cls}')
                out_img = image
        
        self.process_time = time.time() - start_time
        return out_img

    @staticmethod
    def _generate_trimap(mask: np.array,
                         eroision_iter: int = 6, 
                         dilate_iter: int = 8) -> np.array:
        """
        https://stackoverflow.com/questions/57294609/how-to-generate-a-trimap-image
        """
        d_kernel = np.ones((3, 3))
        erode = cv2.erode(mask, d_kernel, iterations=eroision_iter)
        dilate = cv2.dilate(mask, d_kernel, iterations=dilate_iter)
        unknown1 = cv2.bitwise_xor(erode, mask)
        unknown2 = cv2.bitwise_xor(dilate, mask)
        unknowns = cv2.add(unknown1, unknown2)
        unknowns[unknowns == 255] = 127
        trimap = cv2.add(mask, unknowns)
        return trimap
    
    @staticmethod
    def _generate_alpha(image: np.array, 
                        trimap: np.array) -> np.array:
        alpha = estimate_alpha_cf(image/255., trimap/255.)
        return alpha
