import os
import cv2
import wandb

import numpy as np
from PIL import Image
from tqdm import tqdm
from typing import Union

from src.yolo_inferencer import YoloModelInferencer
from src.gfm_inferencer import GFMInferencer
from src.robust_matting_inferencer import RobustMattingInferencer
from src.style_matte_inferencer import StyleMatteInferencer

from modules.GFM.core.evaluate import (calculate_sad_mse_mad_whole_img,
                                       compute_connectivity_loss_whole_image,
                                       compute_gradient_whole_image)

InferenceModelType = Union[YoloModelInferencer, GFMInferencer, RobustMattingInferencer, StyleMatteInferencer]


def evaluate_model(model_obj: InferenceModelType,
                   dataset_path: str,
                   model_key: str,
                   image_scale: Union[float, int] = 1,
                   safe_img_pred: bool = False) -> None:
    wandb.init(project="AM_2K_eval", name=f'{model_key}_scale-{image_scale}')
    wandb.define_metric('image_id')
    wandb.define_metric('sad', step_metric='image_id')
    wandb.define_metric('mse', step_metric='image_id')
    wandb.define_metric('mad', step_metric='image_id')
    wandb.define_metric('conn', step_metric='image_id')
    wandb.define_metric('grad', step_metric='image_id')

    orig_img_list = os.listdir(os.path.join(dataset_path, 'original'))
    for i, img_name in tqdm(enumerate(orig_img_list)):
        
        img_path = os.path.join(dataset_path, 'original', img_name)
        alpha_path = os.path.join(dataset_path, 'mask', f"{img_name.split('.')[0]}.png")
        if image_scale == 1:
            img = cv2.imread(img_path)
            alpha = cv2.imread(alpha_path, 0)
        else:
            img = cv2.resize(cv2.imread(img_path), None, fx=scale, fy=scale)
            alpha = cv2.resize(cv2.imread(alpha_path, 0), None, fx=scale, fy=scale)

        out = model_obj.single_inference(img)
        if safe_img_pred:
            save_path = os.path.join(dataset_path, f'{model_key}_scale-{image_scale}_predictions')
            os.makedirs(save_path, exist_ok=True)
            cv2.imwrite(os.path.join(save_path, img_name), out)
        
        predict = model_obj.get_alpha_mask()
        time = model_obj.get_process_time()
        
        alpha = alpha/255.
        predict = predict/255.
        
        img = img[:, :, :3] if img.ndim > 2 else img
        alpha = alpha[:, :, 0] if alpha.ndim > 2 else alpha

        sad_diff, mse_diff, mad_diff = calculate_sad_mse_mad_whole_img(predict, alpha)
        conn_diff = compute_connectivity_loss_whole_image(predict, alpha)
        grad_diff = compute_gradient_whole_image(predict, alpha)
        
        log_dict = {
            'image_id': i,
            'perf_time': time,
            'sad': sad_diff,
            'mse': mse_diff, 
            'mad': mad_diff,
            'conn': conn_diff,
            'grad': grad_diff,
        }
        wandb.log(log_dict)
    wandb.finish()


if __name__ == '__main__':
    dataset_path = './data/validation'
    test_scales = [0.75, 0.5, 0.25]
    for scale in test_scales:
        yolo_w = './weights/yolov8n-seg.pt'
        yolo_obj = YoloModelInferencer(yolo_w)
        evaluate_model(yolo_obj, dataset_path, 'YOLO', scale, True)
        del yolo_obj

        gfm_w = './weights/gfm_r34_tt.pth'
        gfm_obj = GFMInferencer(gfm_w)
        evaluate_model(gfm_obj, dataset_path, 'GFM', scale, True)
        del gfm_obj

        robust_matting_w = './weights/rvm_resnet50.pth'
        rm_obj = RobustMattingInferencer(robust_matting_w)
        evaluate_model(rm_obj, dataset_path, 'RM', scale, True)
        del rm_obj

        style_matting_w = './weights/stylematte_pure.pth'
        sm_obj = StyleMatteInferencer(style_matting_w)
        evaluate_model(sm_obj, dataset_path, 'SM', scale, True)
        del sm_obj
