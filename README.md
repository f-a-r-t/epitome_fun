# Test Task

Please be aware that source code was tested on Mac M1 with python==3.11


# Data and Weights 

Weights used in this repository can be found: https://drive.google.com/drive/folders/1x_BefJguXIC1tzux7OU_U01EWr33YVJB?usp=drive_link

Validataion data used in model_eval_stats.py can be found: https://drive.google.com/drive/folders/19sxESDc-neWy3DkczNEXH0u3iXUPRCiI?usp=sharing


