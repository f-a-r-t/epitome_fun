import os
import cv2
import warnings
import numpy as np
from typing import Union

from src.gfm_inferencer import GFMInferencer
from src.style_matte_inferencer import StyleMatteInferencer
from src.yolo_inferencer import YoloModelInferencer

MAX_SIZE = 3000
MIN_SIZE = 100


def system_breaker(in_dir: str,
                   out_dir: str,
                   model_object: Union[StyleMatteInferencer, GFMInferencer, YoloModelInferencer],
                   threshold: float = 0.15) -> None:
    if not os.path.exists(in_dir):
        raise ValueError(f'path {in_dir} does not exist')
    if not os.path.exists(out_dir):
        warnings.warn(f'Path {out_dir} does not exists. Creating')
        os.makedirs(out_dir)
    
    for img_file in os.listdir(in_dir):
        if img_file.endswith(('.jpg', '.jpeg', '.png', '.JPG', '.JPEG', '.PNG')):
            img = cv2.imread(os.path.join(in_dir, img_file))
            h, w, _ = img.shape
            if max(h, w) >= MAX_SIZE:
                warnings.warn(f'Skipping. File {os.path.join(in_dir, img_file)} is too big')
            elif min(h, w) <= MIN_SIZE:
                warnings.warn(f'Skipping. File {os.path.join(in_dir, img_file)} is too small')
            else:
                if isinstance(model_object, YoloModelInferencer):
                    out = model_object.single_inference(img, generate_alpha=True, threshold=threshold)
                else:
                    out = model_object.single_inference(img, threshold=threshold)
                msk = model_object.get_alpha_mask()
                print(f'Inference time {model_object.get_process_time()} for {os.path.join(in_dir, img_file)}')
                cv2.imwrite(os.path.join(out_dir, img_file),
                            np.concatenate((img, out, cv2.merge([msk, msk, msk])), axis=1))
        else:
            warnings.warn(f'Skipping. File {os.path.join(in_dir, img_file)} not supported')


if __name__ == '__main__':
    yolo_w = './weights/yolov8n-seg.pt'
    yolo_obj = YoloModelInferencer(yolo_w)
    system_breaker('./data/breaking_bad_data',
                   './data/breaking_bad_data_YOLO',
                   yolo_obj)
    del yolo_obj

    style_matting_w = './weights/stylematte_pure.pth'
    sm_obj = StyleMatteInferencer(style_matting_w)
    system_breaker('./data/breaking_bad_data',
                   './data/breaking_bad_data_SM',
                   sm_obj)
    del sm_obj

    gfm_w = './weights/gfm_r34_tt.pth'
    gfm_obj = GFMInferencer(gfm_w)
    system_breaker('./data/breaking_bad_data',
                   './data/breaking_bad_data_GFM',
                   gfm_obj)
    del gfm_obj
