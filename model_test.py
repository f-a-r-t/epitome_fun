import cv2

from src.gfm_inferencer import GFMInferencer
from src.style_matte_inferencer import StyleMatteInferencer
from src.robust_matting_inferencer import RobustMattingInferencer
from src.yolo_inferencer import YoloModelInferencer, YoloMaskMergeTypes

if __name__ == '__main__':
    img_path = './example_pic_cat.jpg'
    img = cv2.imread(img_path)
    
    yolo_w = './weights/yolov8n-seg.pt'
    yolo_obj = YoloModelInferencer(yolo_w)
    
    # if you would like to matt everything that is detected, use below
    # out = yolo_obj.single_inference(img)
    
    # if you would like to have class based matting use, 15 - cat
    out = yolo_obj.class_based_inference(img, 15, YoloMaskMergeTypes.MAX_SCORE, True)
    alpha = yolo_obj.get_alpha_mask()
    print(yolo_obj.get_process_time())
    cv2.imwrite('./output_tst/yolo_out.jpg', out)
    cv2.imwrite('./output_tst/yolo_bg_alpha.jpg', alpha)
    del yolo_obj

    robust_matting_w = './weights/rvm_resnet50.pth'
    rm_obj = RobustMattingInferencer(robust_matting_w)
    out = rm_obj.single_inference(img)
    alpha = rm_obj.get_alpha_mask()
    print(rm_obj.get_process_time())
    cv2.imwrite('./output_tst/rm_out.jpg', out)
    cv2.imwrite('./output_tst/rm_bg_alpha.jpg', alpha)
    del rm_obj
    
    gfm_w = './weights/gfm_r34_tt.pth'
    gfm_obj = GFMInferencer(gfm_w)
    out = gfm_obj.single_inference(img)
    alpha = gfm_obj.get_alpha_mask()
    print(gfm_obj.get_process_time())
    cv2.imwrite('./output_tst/gfm_out.jpg', out)
    cv2.imwrite('./output_tst/gfm_bg_alpha.jpg', alpha)
    del gfm_obj
    
    style_matting_w = './weights/stylematte_pure.pth'
    sm_obj = StyleMatteInferencer(style_matting_w)
    out = sm_obj.single_inference(img, threshold=0.3)
    alpha = sm_obj.get_alpha_mask()
    print(sm_obj.get_process_time())
    cv2.imwrite('./output_tst/sm_out.jpg', out)
    cv2.imwrite('./output_tst/sm_bg_alpha.jpg', alpha)
    del sm_obj
